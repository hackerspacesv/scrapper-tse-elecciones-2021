import pandas as pd
import requests
import json

municipios = pd.read_csv('base_dpto.csv')

base_url = "https://app-div2021-canadacentral-web-06.azurewebsites.net/asamblea-legislativa/departamental/{}/1091"

output_file = open("tse_dump_dptos_1091.json", "w+")

output_file.write('[')
for i in range(0,len(municipios)):
  print("{}".format(municipios['DPTO'][i]))
  response = requests.get(base_url.format(municipios['CDPTO'][i]))
  for line in response.text.splitlines():
    if line.find('var dataGraph') != -1:
      data_table_json = line.split("=")[1].strip().replace(';','')
      data_table = json.loads(data_table_json)
      entry = {
        'CDPTO': str(municipios['CDPTO'][i]),
        'DPTO': municipios['DPTO'][i],
        'data': data_table
      }
      print(str(entry))
      output_file.write(json.dumps(entry)+",\n")
      output_file.flush()
output_file.write(']')
output_file.close()
