import pandas as pd
import requests
import json

municipios = pd.read_csv('base.csv')

base_url = "https://app-div2021-northcentralus-web-02.azurewebsites.net/consejos-municipales/municipal/{}/{}/1042"

output_file = open("tse_dump_municipios_1036.json", "w+")

output_file.write("[\n")
for i in range(0,len(municipios)):
  print("{},{}".format(municipios['DPTO'][i],municipios['MUNIC'][i]))
  response = requests.get(base_url.format(municipios['CDPTO'][i],municipios['CMUN'][i]))
  for line in response.text.splitlines():
    if line.find('dataGraph = ') != -1:
      data_table_json = line.split("=")[1].strip().replace(';','')
      data_table = json.loads(data_table_json)
      entry = {
        'CDPTO': str(municipios['CDPTO'][i]),
        'CMUN': str(municipios['CMUN'][i]),
        'DPTO': municipios['DPTO'][i],
        'MUNIC': municipios['MUNIC'][i],
        'data': data_table
      }
      print(str(entry))
      output_file.write(json.dumps(entry)+",\n")
      output_file.flush()
output_file.write("]")
output_file.close()
